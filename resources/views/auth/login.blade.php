@extends('layouts.app')

@section('content')
<div class="login-container">
    <div class="login-wrapper bg-white">
        <div class="text-center">
            <img src="{{ asset('assets/img/logo-indonesia-marathon.png') }}" class="my-3" alt="" />
        </div>
        <div class="mt-5">
            <h4 class="mb-0 orange-1">Login</h4>
            <p class="mt-0"><small>Welcome back, please login into your account</small></p>
        </div>
        <div class="form-login-wrapper">
            <form class="form-login" action="{{ route('login') }}" method="post"> @csrf
                <div class="form-group mb-2">
                    <label class="text-small mb-1">E-mail</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-white border-none"><i class="icon-mail"></i></span>
                        </div>
                        <input type="text"
                            class="form-control @error('email') is-invalid @enderror border-left-none box-shadow-none border-grey"
                            placeholder="E-mail" name="email" value="{{ old('email') }}" autofocus="email"
                            autocomplete="off">
                    </div>
                </div>
                <div class="form-group mb-2">
                    <label class="text-small mb-1">Password</label>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <span class="input-group-text bg-white border-none"><i class="icon-lock"></i></span>
                        </div>
                        <input type="password"
                            class="form-control @error('password') is-invalid @enderror border-left-none box-shadow-none border-grey"
                            placeholder="Password" name="password">
                    </div>
                </div>

                <div class="d-flex justify-content-between">
                    <div class="form-group form-check">
                        <input type="checkbox" class="form-check-input" id="remember" name="remember"
                            {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-check-label text-small" for="remember">Remember me</label>
                    </div>

                </div>
                <div class="form-group">
                    <!-- <button href="dashboard.html" class="btn btn-block btn-shadhow btn-warning text-white relative btn-login">Login <i class='bx bx-right-arrow-alt'></i></button> -->
                    <button class="btn btn-block btn-shadow btn-dark btn-round text-white relative btn-login">Login
                        <i class='bx bx-right-arrow-alt'></i></button>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection