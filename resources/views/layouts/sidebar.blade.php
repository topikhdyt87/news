<!-- Sidebar -->
<div class="main-menu main-menu-sidebar">
    <div class="logo text-center">
        <img src="{{ asset('assets/img/logo-indonesia-marathon.png') }}" alt="Logo" width="130">
    </div>
    <div class="nav-menu">
        <ul class="nav flex-column">

            <li class="nav-item my-2 active">
                <a href="{{ route('home') }}">
                    <i class="icon-home"></i> Home
                </a>
            </li>

            <li class="menu-title">Data</li>

            <li class="nav-item my-2 ">
                <a href="{{ route('post.create') }}">
                    <i class="bx bx-plus-circle"></i> Add New Post
                </a>
            </li>

            <li class="nav-item my-2 ">
                <a href="{{ route('post.index') }}">
                    <i class="icon-send"></i> All Posts
                </a>
            </li>

            <li class="nav-item my-2 ">
                <a href="{{ route('category.index') }}">
                    <i class="icon-tag"></i> Category
                </a>
            </li>


            <li class="menu-title">User</li>

            <li class="nav-item my-2 ">
                <a href="#" data-toggle="modal" data-target="#logoutModal">
                    <i class="icon-log-out"></i> Logout
                </a>
            </li>

        </ul>
    </div>
</div>