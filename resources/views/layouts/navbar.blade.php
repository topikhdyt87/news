<!-- Navbar Top -->
<div class="navbar fixed-top navbar-main">
    <a href="javascript:;" class="toggle-sidebar"><i class="icon-code"></i></a>

    <div class="mr-auto navbar-title">
    </div>

    <div class="ml-auto d-flex justify-content-end align-items-center">

        <div class="d-flex dropdown">

        </div>
        <div class="dropdown">
            <a class="navbar-user-info toggle-dropdown" href="javascript:;"><img
                    src="{{ asset('assets/img/avatar_2.jpeg') }}" alt="user" class="rounded-circle" width="35" /></a>
            <ul class="dropdown-wrapper dropdown-menu-right dropdown-menu-user">
                <li>
                    <a class="dropdown-item" href="#"><i class="icon-user"></i> Edit Profile</a>
                </li>
                <li>
                    <div class="dropdown-divider"></div>
                </li>
                <li>
                    <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal"><i
                            class="icon-power"></i> Logout</a>
                </li>
            </ul>
        </div>
    </div>
</div>


<div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    style="display: none;" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
            <div class="modal-footer">
                <button class="btn btn-danger" type="button" data-dismiss="modal">Cancel</button>
                <form action="{{ route('logout') }}" method="post">
                    @csrf
                    <button class="btn btn-dark">Logout</button>
                </form>
            </div>
        </div>
    </div>
</div>