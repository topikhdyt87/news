@extends('layouts.main')

@section('title', 'Posts')


@section('content')
<main class="main-content">
    <div class="breadcrumb d-flex align-items-center pd-20 pb-0">
        <div>
            <a href="{{ route('home') }}">
                <i class="icon-home"></i> Home
            </a>
        </div>

        <div class="breadcrumb-sep">
            <i class="icon-chevron-right"></i>
        </div>

        <div>
            Post
        </div>

    </div>
    <div class="pd-20 pt-0">
        <!-- card -->
        <div class="card mt-3">
            <!-- card body -->
            <div class="card-body">
                <h6 class="card-title mb-4">Post</h6>
                <hr style="border: 1px solid #1B1D4E;">

                <div class="row overflow-auto">
                    <div class="col-lg">
                        <table class="table table-hover text-center">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">image</th>
                                    <th scope="col">Title</th>
                                    <th scope="col">Category</th>
                                    <th scope="col">Created At</th>
                                    <th scope="col" colspan="2">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($post as $posts)
                                <tr>
                                    <th scope="row">{{ $loop->iteration }}</th>
                                    <th><img src="{{ $posts->posts_image }}" width="60px" alt="">
                                    </th>
                                    <td>{{ $posts->title }}</td>
                                    <td>{{ $posts->category->title }}</td>
                                    <td>{{ $posts->created_at->format('d M yy') }}</td>
                                    <td>
                                        <a href="{{ url('post') . '/' .  $posts->id . '/edit' }}"
                                            class="btn btn-primary btn-sm">Edit</a>
                                    </td>
                                    <td>
                                        <form action="{{ route('post.destroy',  $posts->id)  }}"
                                            id="formDelete-post{{ $posts->id }}" method="post">
                                            @method('delete')
                                            @csrf
                                            <button type="button" class="btn btn-danger btn-sm"
                                                onclick="deleteData({{ $posts->id }})">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>


            </div>
            <!-- end of card body -->
        </div>
        <!-- end of card -->
    </div>
</main>
@endsection

@section('js')
<script>
    function deleteData(id) {
            Swal.fire({
                title: 'Apakah Anda Yakin?',
                text: "Data yang dihapus tidak dapat dikembalikan!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Hapus!',
                cancelButtonText: 'Kembali'
            }).then((result) => {
                if (result.value) {
                    $('#formDelete-post' + id).submit();
                }
            })
        }
</script>
@endsection