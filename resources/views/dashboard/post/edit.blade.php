@extends('layouts.main')

@section('title', 'Posts')


@section('content')
<main class="main-content">
    <div class="breadcrumb d-flex align-items-center pd-20 pb-0">

        <div>
            <a href="{{ route('home') }}">
                <i class="icon-home"></i> Home
            </a>
        </div>

        <div class="breadcrumb-sep">
            <i class="icon-chevron-right"></i>
        </div>

        <div>
            <a href="{{ route('post.index') }}">Post</a>
        </div>

        <div class="breadcrumb-sep">
            <i class="icon-chevron-right"></i>
        </div>

        <div>Edit Data Post</div>

    </div>

    <!-- content -->
    <div class="pd-20 pt-0">
        <!-- Main Card -->
        <div class="card mt-3">
            <!-- Card Body  -->
            <div class="card-body">
                <h6 class="card-title mb-4">Edit Data Post</h6>
                <hr style="border: 1px solid #1B1D4E;">


                <form action="{{ url('post') . '/' . $post->id }} " method="POST" enctype="multipart/form-data">
                    @csrf @method('put')
                    <!-- If Form Errors -->
                    @if($errors->any())
                    <div class="row">
                        <div class="col-12">
                            <div class="alert alert-danger fade show" role="alert">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    @endif
                    <!-- If Form Errors -->

                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-12 col-lg-8">
                            <div class="card shadow-md">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="title">Title</label>
                                        <input type="text" class="form-control" id="title"
                                            placeholder="Insert title post" name="title" value="{{ $post->title }}">
                                    </div>

                                    <div class="form-group">
                                        <textarea class="form-control" id="summernote" rows="20" name="content">
                                                {{ $post->content }}
                                        </textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end of col -->

                        <!-- col -->
                        <div class="col-12 col-lg-4 mt-3 mt-lg-0">
                            <!-- card -->
                            <div class="card">
                                <!-- card body -->
                                <div class="card-body">
                                    @foreach ($category as $categories)
                                    <div class="form-check my-2">
                                        @if($categories->id == $post->category_id)
                                        <input class="form-check-input" type="radio" name="category_id"
                                            value="{{ $categories->id }}" checked>
                                        @else
                                        <input class="form-check-input" type="radio" name="category_id"
                                            value="{{ $categories->id }}">
                                        @endif
                                        <label class="form-check-label" for="exampleRadios1">
                                            {{ $categories->title }}
                                        </label>
                                    </div>
                                    @endforeach
                                </div>
                                <!-- end of card body -->
                            </div>
                            <!-- end of card -->

                            <!-- card -->
                            <div class="card mt-4">
                                <!-- card body -->
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="posts_image">Image</label>
                                        <input type="file" class="form-control-file" id="posts_image" name="posts_image"
                                            onchange="readURL(this)" value="{{ $post->posts_image }}">
                                    </div>
                                    <img src="{{ $post->posts_image }}" alt="" id="img" class="img-fluid">
                                </div>
                                <!-- end of card body -->
                            </div>
                            <!-- end of card -->

                            <button type="submit" class="btn btn-dark btn-round-left w-100 my-3">
                                Update Content
                            </button>
                        </div>
                        <!-- end of col -->
                    </div>
                    <!-- end of row -->
                </form>
            </div>
            <!-- End Of Card Body -->
        </div>
        <!-- end of Main Card -->
    </div>
    <!-- End Of Content -->
</main>
@endsection

@section('js')
<script>
    $(function() {
        setTimeout(function() {
        $('.alert').alert('close')
        },5000)

        $('#summernote').summernote({
            height: 250,
            maximumImageFileSize: 2097152,
            toolbar: [
                ['style', ['bold', 'italic', 'underline']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['insert', ['picture', 'link']], 
                ["view", ["fullscreen", "codeview"]]
            ]
        });
    });
        function readURL(input) {
            var url = input.value;
            var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
        if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
            var reader = new FileReader();
            reader.onload = function (e) {
            $('#img').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@endsection