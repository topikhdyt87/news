@extends('layouts.main')

@section('title', 'Category')



@section('content')
<main class="main-content">
    <div class="breadcrumb d-flex align-items-center pd-20 pb-0">
        <div>
            <a href="{{ route('home') }}">
                <i class="icon-home"></i> Home
            </a>
        </div>

        <div class="breadcrumb-sep">
            <i class="icon-chevron-right"></i>
        </div>

        <div>Category</div>
    </div>

    <!-- Main content -->
    <div class="pd-20 pt-0">
        <!-- card -->
        <div class="card mt-3">
            <!-- card body -->
            <div class="card-body">
                <h6 class="card-title mb-4">Category</h6>
                <hr style="border: 1px solid #1B1D4E;">

                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-12 col-lg-8">

                        @error('title')
                        <div class="alert alert-danger" role="alert">
                            {{ $message }}
                        </div>
                        @enderror

                        <form action="{{ route('category.store') }}" method="post" class="mb-4">
                            @csrf
                            <div class="input-group">
                                <input type="text" class="form-control" name="title" required
                                    placeholder="Category Name">
                                <div class="input-group-append">
                                    <button class="btn btn-dark btn-round border-0" type="submit">Add</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- end of col -->
                </div>
                <!-- end of row -->

                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-12  col-lg-8 overflow-auto">
                        <table class="table table-bordered table-hover text-center">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Category Name</th>
                                    <th scope="col">Created At</th>
                                    <th scope="col" colspan="2">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($category as $categories)
                                <tr>
                                    <th scope="row">{{ $loop->iteration }}</th>
                                    <td>{{ $categories->title }}</td>
                                    <td>{{ $categories->created_at->format('d M yy') }}</td>
                                    <td>
                                        <button class="btn btn-primary btn-sm" id="btnUpdate-category"
                                            data-title="{{ $categories->title }}"
                                            data-id='{{ $categories->id }}'>Update</button>
                                    </td>
                                    <td>
                                        <form action="{{ url('/category') . '/' . $categories->id }}"
                                            id="formDelete-category{{ $categories->id }}" method="post">
                                            @method('delete')
                                            @csrf
                                            <button type="button" class="btn btn-danger btn-sm"
                                                onclick="deleteData({{ $categories->id }})">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        <div class="float-right">
                            {{ $category->links() }}
                        </div>
                    </div>
                    <!-- end of col -->
                </div>
                <!-- end of row -->
            </div>
            <!-- end card body -->
        </div>
        <!-- end of card -->
    </div>
    <!-- end of main content -->
</main>

<form id="formUpdate-category" method="post" class="mb-4">
    <div class="modal fade" id="modalUpdate-category" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog  modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Update Data Category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @method('put')
                    @csrf
                    <input type="hidden" name="id" id="update-id">
                    <div class="input-group ">
                        <input type="text" class="form-control" name="title" id="update-title" required
                            placeholder="Category Name">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Edit</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
@section('js')
<script>
    $(function( ) {
        $(document).on('click','#btnUpdate-category', function() {
            $('#update-id').val($(this).attr('data-id'));
            $('#update-title').val($(this).attr('data-title'));
            $('#formUpdate-category').attr('action','/category/' + $(this).attr('data-id'));
            $('#modalUpdate-category').modal('show');
        }); 
    });

    function deleteData(id) {
        Swal.fire({
            title: 'Apakah Anda Yakin?',
            text: "Data yang dihapus tidak dapat dikembalikan!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Ya, Hapus!',
            cancelButtonText: 'Kembali'
        }).then((result) => {
            if (result.value) {
                $('#formDelete-category' + id).submit();
            }
        })
    }
</script>
@endsection