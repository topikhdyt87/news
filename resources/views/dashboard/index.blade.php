@extends('layouts.main')

@section('title', 'Dashboard')

@section('content')
<main class="main-content">
    <div class="dashboard pd-20">

        <!-- row -->
        <div class="row">
            <!-- col -->
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card shadow py-2" style="border-left: 2px solid #96ABFF">
                    <div class="card-body text-center">
                        <div class="row no-gutters">
                            <div class="col mr-2">
                                <div class="font-weight-bold text-primary text-uppercase mb-3">
                                    Total Posts
                                </div>
                                <div class="h5 mb-0 font-weight-bold ">{{ $post }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end of col -->

            <!-- col -->
            <div class="col-xl-4 col-md-6 mb-4">
                <div class="card shadow py-2" style="border-left: 2px solid #30D271">
                    <div class="card-body text-center">
                        <div class="row no-gutters">
                            <div class="col mr-2">
                                <div class="font-weight-bold text-success text-uppercase mb-3">
                                    Total Category
                                </div>
                                <div class="h5 mb-0 font-weight-bold ">{{ $category }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end of col -->

            <!-- col -->
            <div class="col-xl-4 col-12 mb-4">
                <div class="card  shadow py-2" style="border-left: 2px solid #FFA843">
                    <div class="card-body text-center">
                        <div class="row no-gutters">
                            <div class="col mr-2">
                                <div class="font-weight-bold text-warning text-uppercase mb-3">
                                    Visitor Today (By Page)
                                </div>
                                <div class="h5 mb-0 font-weight-bold">1</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end of col -->

        </div>
        <!-- end of row -->


        <div class="row mt-4">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-7">
                                <h5 class="mb-0">2.7 K <small class="text-grey">(2.780)</small></h5>
                                <div>Avg Visitor</div>

                                <div class="mt-4 mb-2">
                                    <div><small>Direct URL: 28K</small></div>
                                    <div class="progress progress-xs">
                                        <div class="progress-bar bg-main" role="progressbar" style="width: 28%"
                                            aria-valuenow="28" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>

                                    <div class="mt-3"><small>Social Media: 58K</small></div>
                                    <div class="progress progress-xs">
                                        <div class="progress-bar" role="progressbar" style="width: 58%"
                                            aria-valuenow="58" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>

                                    <div class="mt-3"><small>Referal: 75K</small></div>
                                    <div class="progress progress-xs">
                                        <div class="progress-bar bg-warning" role="progressbar" style="width: 75%"
                                            aria-valuenow="75" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>

                                    <div class="mt-3"><small>Ads: 90K</small></div>
                                    <div class="progress progress-xs">
                                        <div class="progress-bar bg-success" role="progressbar" style="width: 90%"
                                            aria-valuenow="90" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>

                                </div>
                            </div>

                            <div class="col-5">
                                <div id="sparkBar" style="min-height: 180px;">
                                    <div id="apexchartsy27bmse4"
                                        class="apexcharts-canvas apexchartsy27bmse4 apexcharts-theme-light"
                                        style="width: 129px; height: 180px;"><svg id="SvgjsSvg1272" width="129"
                                            height="180" xmlns="http://www.w3.org/2000/svg" version="1.1"
                                            xmlns:xlink="http://www.w3.org/1999/xlink"
                                            xmlns:svgjs="http://svgjs.com/svgjs" class="apexcharts-svg"
                                            xmlns:data="ApexChartsNS" transform="translate(0, 0)"
                                            style="background: transparent;">
                                            <g id="SvgjsG1274" class="apexcharts-inner apexcharts-graphical"
                                                transform="translate(16.166666666666668, 0)">
                                                <defs id="SvgjsDefs1273">
                                                    <linearGradient id="SvgjsLinearGradient1277" x1="0" y1="0" x2="0"
                                                        y2="1">
                                                        <stop id="SvgjsStop1278" stop-opacity="0.4"
                                                            stop-color="rgba(216,227,240,0.4)" offset="0"></stop>
                                                        <stop id="SvgjsStop1279" stop-opacity="0.5"
                                                            stop-color="rgba(190,209,230,0.5)" offset="1"></stop>
                                                        <stop id="SvgjsStop1280" stop-opacity="0.5"
                                                            stop-color="rgba(190,209,230,0.5)" offset="1"></stop>
                                                    </linearGradient>
                                                    <clipPath id="gridRectMasky27bmse4">
                                                        <rect id="SvgjsRect1282" width="133" height="180"
                                                            x="-14.166666666666668" y="0" rx="0" ry="0" opacity="1"
                                                            stroke-width="0" stroke="none" stroke-dasharray="0"
                                                            fill="#fff"></rect>
                                                    </clipPath>
                                                    <clipPath id="gridRectMarkerMasky27bmse4">
                                                        <rect id="SvgjsRect1283" width="108.66666666666666" height="184"
                                                            x="-2" y="-2" rx="0" ry="0" opacity="1" stroke-width="0"
                                                            stroke="none" stroke-dasharray="0" fill="#fff"></rect>
                                                    </clipPath>
                                                </defs>
                                                <rect id="SvgjsRect1281" width="8.722222222222221" height="180" x="0"
                                                    y="0" rx="0" ry="0" opacity="1" stroke-width="0"
                                                    stroke-dasharray="3" fill="url(#SvgjsLinearGradient1277)"
                                                    class="apexcharts-xcrosshairs" y2="180" filter="none"
                                                    fill-opacity="0.9"></rect>
                                                <g id="SvgjsG1295" class="apexcharts-xaxis" transform="translate(0, 0)">
                                                    <g id="SvgjsG1296" class="apexcharts-xaxis-texts-g"
                                                        transform="translate(0, -4)"></g>
                                                </g>
                                                <g id="SvgjsG1298" class="apexcharts-grid">
                                                    <g id="SvgjsG1299" class="apexcharts-gridlines-horizontal"
                                                        style="display: none;">
                                                        <line id="SvgjsLine1301" x1="0" y1="0" x2="104.66666666666666"
                                                            y2="0" stroke="#e0e0e0" stroke-dasharray="0"
                                                            class="apexcharts-gridline"></line>
                                                        <line id="SvgjsLine1302" x1="0" y1="60" x2="104.66666666666666"
                                                            y2="60" stroke="#e0e0e0" stroke-dasharray="0"
                                                            class="apexcharts-gridline"></line>
                                                        <line id="SvgjsLine1303" x1="0" y1="120" x2="104.66666666666666"
                                                            y2="120" stroke="#e0e0e0" stroke-dasharray="0"
                                                            class="apexcharts-gridline"></line>
                                                        <line id="SvgjsLine1304" x1="0" y1="180" x2="104.66666666666666"
                                                            y2="180" stroke="#e0e0e0" stroke-dasharray="0"
                                                            class="apexcharts-gridline"></line>
                                                    </g>
                                                    <g id="SvgjsG1300" class="apexcharts-gridlines-vertical"
                                                        style="display: none;"></g>
                                                    <line id="SvgjsLine1306" x1="0" y1="180" x2="104.66666666666666"
                                                        y2="180" stroke="transparent" stroke-dasharray="0"></line>
                                                    <line id="SvgjsLine1305" x1="0" y1="1" x2="0" y2="180"
                                                        stroke="transparent" stroke-dasharray="0"></line>
                                                </g>
                                                <g id="SvgjsG1285" class="apexcharts-bar-series apexcharts-plot-series">
                                                    <g id="SvgjsG1286" class="apexcharts-series" rel="1"
                                                        seriesName="Session" data:realIndex="0">
                                                        <path id="SvgjsPath1288"
                                                            d="M -4.361111111111111 180L -4.361111111111111 107.18055555555556Q 0 102.81944444444444 4.361111111111111 107.18055555555556L 4.361111111111111 107.18055555555556L 4.361111111111111 180L 4.361111111111111 180z"
                                                            fill="rgba(204,204,204,0.85)" fill-opacity="1"
                                                            stroke-opacity="1" stroke-linecap="square" stroke-width="0"
                                                            stroke-dasharray="0" class="apexcharts-bar-area" index="0"
                                                            clip-path="url(#gridRectMasky27bmse4)"
                                                            pathTo="M -4.361111111111111 180L -4.361111111111111 107.18055555555556Q 0 102.81944444444444 4.361111111111111 107.18055555555556L 4.361111111111111 107.18055555555556L 4.361111111111111 180L 4.361111111111111 180z"
                                                            pathFrom="M -4.361111111111111 180L -4.361111111111111 180L 4.361111111111111 180L 4.361111111111111 180L 4.361111111111111 180L -4.361111111111111 180"
                                                            cy="105" cx="4.361111111111111" j="0" val="25"
                                                            barHeight="75" barWidth="8.722222222222221"></path>
                                                        <path id="SvgjsPath1289"
                                                            d="M 13.083333333333332 180L 13.083333333333332 77.18055555555556Q 17.444444444444443 72.81944444444444 21.805555555555554 77.18055555555556L 21.805555555555554 77.18055555555556L 21.805555555555554 180L 21.805555555555554 180z"
                                                            fill="rgba(204,204,204,0.85)" fill-opacity="1"
                                                            stroke-opacity="1" stroke-linecap="square" stroke-width="0"
                                                            stroke-dasharray="0" class="apexcharts-bar-area" index="0"
                                                            clip-path="url(#gridRectMasky27bmse4)"
                                                            pathTo="M 13.083333333333332 180L 13.083333333333332 77.18055555555556Q 17.444444444444443 72.81944444444444 21.805555555555554 77.18055555555556L 21.805555555555554 77.18055555555556L 21.805555555555554 180L 21.805555555555554 180z"
                                                            pathFrom="M 13.083333333333332 180L 13.083333333333332 180L 21.805555555555554 180L 21.805555555555554 180L 21.805555555555554 180L 13.083333333333332 180"
                                                            cy="75" cx="21.805555555555554" j="1" val="35"
                                                            barHeight="105" barWidth="8.722222222222221"></path>
                                                        <path id="SvgjsPath1290"
                                                            d="M 30.527777777777775 180L 30.527777777777775 17.180555555555557Q 34.888888888888886 12.819444444444446 39.25 17.180555555555557L 39.25 17.180555555555557L 39.25 180L 39.25 180z"
                                                            fill="rgba(255,108,76,0.85)" fill-opacity="1"
                                                            stroke-opacity="1" stroke-linecap="square" stroke-width="0"
                                                            stroke-dasharray="0" class="apexcharts-bar-area" index="0"
                                                            clip-path="url(#gridRectMasky27bmse4)"
                                                            pathTo="M 30.527777777777775 180L 30.527777777777775 17.180555555555557Q 34.888888888888886 12.819444444444446 39.25 17.180555555555557L 39.25 17.180555555555557L 39.25 180L 39.25 180z"
                                                            pathFrom="M 30.527777777777775 180L 30.527777777777775 180L 39.25 180L 39.25 180L 39.25 180L 30.527777777777775 180"
                                                            cy="15" cx="39.25" j="2" val="55" barHeight="165"
                                                            barWidth="8.722222222222221"></path>
                                                        <path id="SvgjsPath1291"
                                                            d="M 47.972222222222214 180L 47.972222222222214 77.18055555555556Q 52.33333333333333 72.81944444444444 56.694444444444436 77.18055555555556L 56.694444444444436 77.18055555555556L 56.694444444444436 180L 56.694444444444436 180z"
                                                            fill="rgba(204,204,204,0.85)" fill-opacity="1"
                                                            stroke-opacity="1" stroke-linecap="square" stroke-width="0"
                                                            stroke-dasharray="0" class="apexcharts-bar-area" index="0"
                                                            clip-path="url(#gridRectMasky27bmse4)"
                                                            pathTo="M 47.972222222222214 180L 47.972222222222214 77.18055555555556Q 52.33333333333333 72.81944444444444 56.694444444444436 77.18055555555556L 56.694444444444436 77.18055555555556L 56.694444444444436 180L 56.694444444444436 180z"
                                                            pathFrom="M 47.972222222222214 180L 47.972222222222214 180L 56.694444444444436 180L 56.694444444444436 180L 56.694444444444436 180L 47.972222222222214 180"
                                                            cy="75" cx="56.694444444444436" j="3" val="35"
                                                            barHeight="105" barWidth="8.722222222222221"></path>
                                                        <path id="SvgjsPath1292"
                                                            d="M 65.41666666666666 180L 65.41666666666666 92.18055555555556Q 69.77777777777777 87.81944444444444 74.13888888888889 92.18055555555556L 74.13888888888889 92.18055555555556L 74.13888888888889 180L 74.13888888888889 180z"
                                                            fill="rgba(204,204,204,0.85)" fill-opacity="1"
                                                            stroke-opacity="1" stroke-linecap="square" stroke-width="0"
                                                            stroke-dasharray="0" class="apexcharts-bar-area" index="0"
                                                            clip-path="url(#gridRectMasky27bmse4)"
                                                            pathTo="M 65.41666666666666 180L 65.41666666666666 92.18055555555556Q 69.77777777777777 87.81944444444444 74.13888888888889 92.18055555555556L 74.13888888888889 92.18055555555556L 74.13888888888889 180L 74.13888888888889 180z"
                                                            pathFrom="M 65.41666666666666 180L 65.41666666666666 180L 74.13888888888889 180L 74.13888888888889 180L 74.13888888888889 180L 65.41666666666666 180"
                                                            cy="90" cx="74.13888888888889" j="4" val="30" barHeight="90"
                                                            barWidth="8.722222222222221"></path>
                                                        <path id="SvgjsPath1293"
                                                            d="M 82.8611111111111 180L 82.8611111111111 62.18055555555556Q 87.22222222222221 57.81944444444444 91.58333333333331 62.18055555555556L 91.58333333333331 62.18055555555556L 91.58333333333331 180L 91.58333333333331 180z"
                                                            fill="rgba(204,204,204,0.85)" fill-opacity="1"
                                                            stroke-opacity="1" stroke-linecap="square" stroke-width="0"
                                                            stroke-dasharray="0" class="apexcharts-bar-area" index="0"
                                                            clip-path="url(#gridRectMasky27bmse4)"
                                                            pathTo="M 82.8611111111111 180L 82.8611111111111 62.18055555555556Q 87.22222222222221 57.81944444444444 91.58333333333331 62.18055555555556L 91.58333333333331 62.18055555555556L 91.58333333333331 180L 91.58333333333331 180z"
                                                            pathFrom="M 82.8611111111111 180L 82.8611111111111 180L 91.58333333333331 180L 91.58333333333331 180L 91.58333333333331 180L 82.8611111111111 180"
                                                            cy="60" cx="91.58333333333331" j="5" val="40"
                                                            barHeight="120" barWidth="8.722222222222221"></path>
                                                        <path id="SvgjsPath1294"
                                                            d="M 100.30555555555554 180L 100.30555555555554 2.1805555555555554Q 104.66666666666666 -2.1805555555555554 109.02777777777777 2.1805555555555554L 109.02777777777777 2.1805555555555554L 109.02777777777777 180L 109.02777777777777 180z"
                                                            fill="rgba(255,108,76,0.85)" fill-opacity="1"
                                                            stroke-opacity="1" stroke-linecap="square" stroke-width="0"
                                                            stroke-dasharray="0" class="apexcharts-bar-area" index="0"
                                                            clip-path="url(#gridRectMasky27bmse4)"
                                                            pathTo="M 100.30555555555554 180L 100.30555555555554 2.1805555555555554Q 104.66666666666666 -2.1805555555555554 109.02777777777777 2.1805555555555554L 109.02777777777777 2.1805555555555554L 109.02777777777777 180L 109.02777777777777 180z"
                                                            pathFrom="M 100.30555555555554 180L 100.30555555555554 180L 109.02777777777777 180L 109.02777777777777 180L 109.02777777777777 180L 100.30555555555554 180"
                                                            cy="0" cx="109.02777777777777" j="6" val="60"
                                                            barHeight="180" barWidth="8.722222222222221"></path>
                                                    </g>
                                                    <g id="SvgjsG1287" class="apexcharts-datalabels" data:realIndex="0">
                                                    </g>
                                                </g>
                                                <line id="SvgjsLine1307" x1="0" y1="0" x2="104.66666666666666" y2="0"
                                                    stroke="#b6b6b6" stroke-dasharray="0" stroke-width="1"
                                                    class="apexcharts-ycrosshairs"></line>
                                                <line id="SvgjsLine1308" x1="0" y1="0" x2="104.66666666666666" y2="0"
                                                    stroke-dasharray="0" stroke-width="0"
                                                    class="apexcharts-ycrosshairs-hidden"></line>
                                                <g id="SvgjsG1309" class="apexcharts-yaxis-annotations"></g>
                                                <g id="SvgjsG1310" class="apexcharts-xaxis-annotations"></g>
                                                <g id="SvgjsG1311" class="apexcharts-point-annotations"></g>
                                            </g>
                                            <g id="SvgjsG1297" class="apexcharts-yaxis" rel="0"
                                                transform="translate(-18, 0)"></g>
                                            <g id="SvgjsG1275" class="apexcharts-annotations"></g>
                                        </svg>
                                        <div class="apexcharts-legend"></div>
                                        <div class="apexcharts-tooltip apexcharts-theme-light">
                                            <div class="apexcharts-tooltip-series-group"><span
                                                    class="apexcharts-tooltip-marker"
                                                    style="background-color: rgb(204, 204, 204);"></span>
                                                <div class="apexcharts-tooltip-text"
                                                    style="font-family: Helvetica, Arial, sans-serif; font-size: 12px;">
                                                    <div class="apexcharts-tooltip-y-group"><span
                                                            class="apexcharts-tooltip-text-label"></span><span
                                                            class="apexcharts-tooltip-text-value"></span></div>
                                                    <div class="apexcharts-tooltip-z-group"><span
                                                            class="apexcharts-tooltip-text-z-label"></span><span
                                                            class="apexcharts-tooltip-text-z-value"></span></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center mt-3">
                                    <a href="#" class="btn btn-info btn-sm text-white">View Details</a>
                                </div>
                                <div class="resize-triggers">
                                    <div class="expand-trigger">
                                        <div style="width: 160px; height: 236px;"></div>
                                    </div>
                                    <div class="contract-trigger"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title">Tickets</h6>
                        <div class="row mt-3">
                            <div class="col-6">
                                <div class="mt-3 d-flex">
                                    <div class="ph-10 pv-5 mb-0 mt-0 rounded-left bg-main w-50px text-center">120
                                    </div>
                                    <div class="ph-10 pv-5 rounded-right bg-light flex-grow-1"><small>Bronze
                                            Tickets</small></div>
                                </div>
                                <div class="mt-3 d-flex">
                                    <div
                                        class="ph-10 pv-5 mb-0 mt-0 rounded-left bg-secondary text-white w-50px text-center">
                                        110</div>
                                    <div class="ph-10 pv-5 rounded-right bg-light flex-grow-1"><small>Silver
                                            Tickets</small></div>
                                </div>
                                <div class="mt-3 d-flex">
                                    <div class="ph-10 pv-5 mb-0 mt-0 rounded-left bg-warning w-50px text-center">90
                                    </div>
                                    <div class="ph-10 pv-5 rounded-right bg-light flex-grow-1"><small>Gold
                                            Tickets</small></div>
                                </div>
                                <div class="mt-3 d-flex">
                                    <div
                                        class="ph-10 pv-5 mb-0 mt-0 rounded-left bg-info text-white w-50px text-center">
                                        40</div>
                                    <div class="ph-10 pv-5 rounded-right bg-light flex-grow-1"><small>Platinum
                                            Tickets</small></div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div id="ticketChart" style="min-height: 168.7px;">
                                    <div id="apexchartszah8dans"
                                        class="apexcharts-canvas apexchartszah8dans apexcharts-theme-light"
                                        style="width: 160px; height: 168.7px;"><svg id="SvgjsSvg1312" width="160"
                                            height="168.70000000000002" xmlns="http://www.w3.org/2000/svg" version="1.1"
                                            xmlns:xlink="http://www.w3.org/1999/xlink"
                                            xmlns:svgjs="http://svgjs.com/svgjs" class="apexcharts-svg"
                                            xmlns:data="ApexChartsNS" transform="translate(0, 0)"
                                            style="background: transparent;">
                                            <g id="SvgjsG1314" class="apexcharts-inner apexcharts-graphical"
                                                transform="translate(-2, 0)">
                                                <defs id="SvgjsDefs1313">
                                                    <clipPath id="gridRectMaskzah8dans">
                                                        <rect id="SvgjsRect1316" width="172" height="190" x="-3" y="-1"
                                                            rx="0" ry="0" opacity="1" stroke-width="0" stroke="none"
                                                            stroke-dasharray="0" fill="#fff"></rect>
                                                    </clipPath>
                                                    <clipPath id="gridRectMarkerMaskzah8dans">
                                                        <rect id="SvgjsRect1317" width="170" height="192" x="-2" y="-2"
                                                            rx="0" ry="0" opacity="1" stroke-width="0" stroke="none"
                                                            stroke-dasharray="0" fill="#fff"></rect>
                                                    </clipPath>
                                                </defs>
                                                <g id="SvgjsG1319" class="apexcharts-radialbar">
                                                    <g id="SvgjsG1320">
                                                        <g id="SvgjsG1321" class="apexcharts-tracks">
                                                            <g id="SvgjsG1322"
                                                                class="apexcharts-radialbar-track apexcharts-track"
                                                                rel="1">
                                                                <path id="apexcharts-radialbarTrack-0"
                                                                    d="M 83 16.77073170731707 A 66.22926829268293 66.22926829268293 0 1 1 82.98844081212974 16.77073271604651"
                                                                    fill="none" fill-opacity="1"
                                                                    stroke="rgba(242,242,242,0.85)" stroke-opacity="1"
                                                                    stroke-linecap="butt"
                                                                    stroke-width="2.4226341463414647"
                                                                    stroke-dasharray="0"
                                                                    class="apexcharts-radialbar-area"
                                                                    data:pathOrig="M 83 16.77073170731707 A 66.22926829268293 66.22926829268293 0 1 1 82.98844081212974 16.77073271604651">
                                                                </path>
                                                            </g>
                                                        </g>
                                                        <g id="SvgjsG1324">
                                                            <g id="SvgjsG1329"
                                                                class="apexcharts-series apexcharts-radial-series"
                                                                seriesName="TicketxSales" rel="1" data:realIndex="0">
                                                                <path id="SvgjsPath1330"
                                                                    d="M 83 16.77073170731707 A 66.22926829268293 66.22926829268293 0 1 1 29.419396426201658 121.92858717256053"
                                                                    fill="none" fill-opacity="0.85"
                                                                    stroke="rgba(0,143,251,0.85)" stroke-opacity="1"
                                                                    stroke-linecap="butt"
                                                                    stroke-width="2.4975609756097574"
                                                                    stroke-dasharray="0"
                                                                    class="apexcharts-radialbar-area apexcharts-radialbar-slice-0"
                                                                    data:angle="234" data:value="65" index="0" j="0"
                                                                    data:pathOrig="M 83 16.77073170731707 A 66.22926829268293 66.22926829268293 0 1 1 29.419396426201658 121.92858717256053">
                                                                </path>
                                                            </g>
                                                            <circle id="SvgjsCircle1325" r="55.0179512195122" cx="83"
                                                                cy="83" class="apexcharts-radialbar-hollow"
                                                                fill="transparent"></circle>
                                                            <g id="SvgjsG1326" class="apexcharts-datalabels-group"
                                                                transform="translate(0, 0) scale(1)"
                                                                style="opacity: 1;"><text id="SvgjsText1327"
                                                                    font-family="Helvetica, Arial, sans-serif" x="83"
                                                                    y="83" text-anchor="middle" dominant-baseline="auto"
                                                                    font-size="16px" font-weight="400" fill="#008ffb"
                                                                    class="apexcharts-text apexcharts-datalabel-label"
                                                                    style="font-family: Helvetica, Arial, sans-serif;">Ticket
                                                                    Sales</text><text id="SvgjsText1328"
                                                                    font-family="Helvetica, Arial, sans-serif" x="83"
                                                                    y="115" text-anchor="middle"
                                                                    dominant-baseline="auto" font-size="14px"
                                                                    font-weight="400" fill="#373d3f"
                                                                    class="apexcharts-text apexcharts-datalabel-value"
                                                                    style="font-family: Helvetica, Arial, sans-serif;">65%</text>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                                <line id="SvgjsLine1331" x1="0" y1="0" x2="166" y2="0" stroke="#b6b6b6"
                                                    stroke-dasharray="0" stroke-width="1"
                                                    class="apexcharts-ycrosshairs"></line>
                                                <line id="SvgjsLine1332" x1="0" y1="0" x2="166" y2="0"
                                                    stroke-dasharray="0" stroke-width="0"
                                                    class="apexcharts-ycrosshairs-hidden"></line>
                                            </g>
                                            <g id="SvgjsG1315" class="apexcharts-annotations"></g>
                                        </svg>
                                        <div class="apexcharts-legend"></div>
                                    </div>
                                </div>
                                <div class="resize-triggers">
                                    <div class="expand-trigger">
                                        <div style="width: 192px; height: 170px;"></div>
                                    </div>
                                    <div class="contract-trigger"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row mt-4">
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body" style="position: relative;">
                        <h6 class="card-title mb-4">Product Order</h6>
                        <div id="chartOrder" class="mb-2" style="min-height: 253.367px;">
                            <div id="apexcharts42zkq1df"
                                class="apexcharts-canvas apexcharts42zkq1df apexcharts-theme-light"
                                style="width: 212px; height: 253.367px;"><svg id="SvgjsSvg1333" width="212"
                                    height="253.36668701171877" xmlns="http://www.w3.org/2000/svg" version="1.1"
                                    xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs"
                                    class="apexcharts-svg" xmlns:data="ApexChartsNS" transform="translate(0, 0)"
                                    style="background: transparent;">
                                    <foreignObject x="0" y="0" width="212" height="253.36668701171877">
                                        <div class="apexcharts-legend apexcharts-align-center position-bottom"
                                            xmlns="http://www.w3.org/1999/xhtml"
                                            style="right: 0px; position: absolute; left: 0px; top: auto; bottom: 5px;">
                                            <div class="apexcharts-legend-series" rel="1" data:collapsed="false"
                                                style="margin: 0px 5px;"><span class="apexcharts-legend-marker" rel="1"
                                                    data:collapsed="false"
                                                    style="background: rgb(255, 108, 76); color: rgb(255, 108, 76); height: 12px; width: 12px; left: 0px; top: 0px; border-width: 0px; border-color: rgb(255, 255, 255); border-radius: 12px;"></span><span
                                                    class="apexcharts-legend-text" rel="1" i="0"
                                                    data:default-text="Bronze" data:collapsed="false"
                                                    style="color: rgb(55, 61, 63); font-size: 12px; font-weight: 400; font-family: Helvetica, Arial, sans-serif;">Bronze</span>
                                            </div>
                                            <div class="apexcharts-legend-series" rel="2" data:collapsed="false"
                                                style="margin: 0px 5px;"><span class="apexcharts-legend-marker" rel="2"
                                                    data:collapsed="false"
                                                    style="background: rgb(108, 117, 125); color: rgb(108, 117, 125); height: 12px; width: 12px; left: 0px; top: 0px; border-width: 0px; border-color: rgb(255, 255, 255); border-radius: 12px;"></span><span
                                                    class="apexcharts-legend-text" rel="2" i="1"
                                                    data:default-text="Silver" data:collapsed="false"
                                                    style="color: rgb(55, 61, 63); font-size: 12px; font-weight: 400; font-family: Helvetica, Arial, sans-serif;">Silver</span>
                                            </div>
                                            <div class="apexcharts-legend-series" rel="3" data:collapsed="false"
                                                style="margin: 0px 5px;"><span class="apexcharts-legend-marker" rel="3"
                                                    data:collapsed="false"
                                                    style="background: rgb(255, 193, 7); color: rgb(255, 193, 7); height: 12px; width: 12px; left: 0px; top: 0px; border-width: 0px; border-color: rgb(255, 255, 255); border-radius: 12px;"></span><span
                                                    class="apexcharts-legend-text" rel="3" i="2"
                                                    data:default-text="Gold" data:collapsed="false"
                                                    style="color: rgb(55, 61, 63); font-size: 12px; font-weight: 400; font-family: Helvetica, Arial, sans-serif;">Gold</span>
                                            </div>
                                            <div class="apexcharts-legend-series" rel="4" data:collapsed="false"
                                                style="margin: 0px 5px;"><span class="apexcharts-legend-marker" rel="4"
                                                    data:collapsed="false"
                                                    style="background: rgb(69, 103, 226); color: rgb(69, 103, 226); height: 12px; width: 12px; left: 0px; top: 0px; border-width: 0px; border-color: rgb(255, 255, 255); border-radius: 12px;"></span><span
                                                    class="apexcharts-legend-text" rel="4" i="3"
                                                    data:default-text="Platinum" data:collapsed="false"
                                                    style="color: rgb(55, 61, 63); font-size: 12px; font-weight: 400; font-family: Helvetica, Arial, sans-serif;">Platinum</span>
                                            </div>
                                        </div>
                                        <style type="text/css">
                                            .apexcharts-legend {
                                                display: flex;
                                                overflow: auto;
                                                padding: 0 10px;
                                            }

                                            .apexcharts-legend.position-bottom,
                                            .apexcharts-legend.position-top {
                                                flex-wrap: wrap
                                            }

                                            .apexcharts-legend.position-right,
                                            .apexcharts-legend.position-left {
                                                flex-direction: column;
                                                bottom: 0;
                                            }

                                            .apexcharts-legend.position-bottom.apexcharts-align-left,
                                            .apexcharts-legend.position-top.apexcharts-align-left,
                                            .apexcharts-legend.position-right,
                                            .apexcharts-legend.position-left {
                                                justify-content: flex-start;
                                            }

                                            .apexcharts-legend.position-bottom.apexcharts-align-center,
                                            .apexcharts-legend.position-top.apexcharts-align-center {
                                                justify-content: center;
                                            }

                                            .apexcharts-legend.position-bottom.apexcharts-align-right,
                                            .apexcharts-legend.position-top.apexcharts-align-right {
                                                justify-content: flex-end;
                                            }

                                            .apexcharts-legend-series {
                                                cursor: pointer;
                                                line-height: normal;
                                            }

                                            .apexcharts-legend.position-bottom .apexcharts-legend-series,
                                            .apexcharts-legend.position-top .apexcharts-legend-series {
                                                display: flex;
                                                align-items: center;
                                            }

                                            .apexcharts-legend-text {
                                                position: relative;
                                                font-size: 14px;
                                            }

                                            .apexcharts-legend-text *,
                                            .apexcharts-legend-marker * {
                                                pointer-events: none;
                                            }

                                            .apexcharts-legend-marker {
                                                position: relative;
                                                display: inline-block;
                                                cursor: pointer;
                                                margin-right: 3px;
                                                border-style: solid;
                                            }

                                            .apexcharts-legend.apexcharts-align-right .apexcharts-legend-series,
                                            .apexcharts-legend.apexcharts-align-left .apexcharts-legend-series {
                                                display: inline-block;
                                            }

                                            .apexcharts-legend-series.apexcharts-no-click {
                                                cursor: auto;
                                            }

                                            .apexcharts-legend .apexcharts-hidden-zero-series,
                                            .apexcharts-legend .apexcharts-hidden-null-series {
                                                display: none !important;
                                            }

                                            .apexcharts-inactive-legend {
                                                opacity: 0.45;
                                            }
                                        </style>
                                    </foreignObject>
                                    <g id="SvgjsG1335" class="apexcharts-inner apexcharts-graphical"
                                        transform="translate(-0.5, 0)">
                                        <defs id="SvgjsDefs1334">
                                            <clipPath id="gridRectMask42zkq1df">
                                                <rect id="SvgjsRect1337" width="221" height="229" x="-3" y="-1" rx="0"
                                                    ry="0" opacity="1" stroke-width="0" stroke="none"
                                                    stroke-dasharray="0" fill="#fff"></rect>
                                            </clipPath>
                                            <clipPath id="gridRectMarkerMask42zkq1df">
                                                <rect id="SvgjsRect1338" width="219" height="231" x="-2" y="-2" rx="0"
                                                    ry="0" opacity="1" stroke-width="0" stroke="none"
                                                    stroke-dasharray="0" fill="#fff"></rect>
                                            </clipPath>
                                        </defs>
                                        <g id="SvgjsG1340" class="apexcharts-radialbar">
                                            <g id="SvgjsG1341">
                                                <g id="SvgjsG1342" class="apexcharts-tracks">
                                                    <g id="SvgjsG1343"
                                                        class="apexcharts-radialbar-track apexcharts-track" rel="1">
                                                        <path id="apexcharts-radialbarTrack-0"
                                                            d="M 107.5 20.953658536585365 A 86.54634146341463 86.54634146341463 0 1 1 107.48489481393575 20.953659854761526"
                                                            fill="none" fill-opacity="1" stroke="rgba(242,242,242,0.85)"
                                                            stroke-opacity="1" stroke-linecap="butt"
                                                            stroke-width="4.741170731707319" stroke-dasharray="0"
                                                            class="apexcharts-radialbar-area"
                                                            data:pathOrig="M 107.5 20.953658536585365 A 86.54634146341463 86.54634146341463 0 1 1 107.48489481393575 20.953659854761526">
                                                        </path>
                                                    </g>
                                                    <g id="SvgjsG1345"
                                                        class="apexcharts-radialbar-track apexcharts-track" rel="2">
                                                        <path id="apexcharts-radialbarTrack-1"
                                                            d="M 107.5 30.84146341463415 A 76.65853658536585 76.65853658536585 0 1 1 107.48662056143617 30.84146458221042"
                                                            fill="none" fill-opacity="1" stroke="rgba(242,242,242,0.85)"
                                                            stroke-opacity="1" stroke-linecap="butt"
                                                            stroke-width="4.741170731707319" stroke-dasharray="0"
                                                            class="apexcharts-radialbar-area"
                                                            data:pathOrig="M 107.5 30.84146341463415 A 76.65853658536585 76.65853658536585 0 1 1 107.48662056143617 30.84146458221042">
                                                        </path>
                                                    </g>
                                                    <g id="SvgjsG1347"
                                                        class="apexcharts-radialbar-track apexcharts-track" rel="3">
                                                        <path id="apexcharts-radialbarTrack-2"
                                                            d="M 107.5 40.72926829268293 A 66.77073170731707 66.77073170731707 0 1 1 107.48834630893657 40.72926930965933"
                                                            fill="none" fill-opacity="1" stroke="rgba(242,242,242,0.85)"
                                                            stroke-opacity="1" stroke-linecap="butt"
                                                            stroke-width="4.741170731707319" stroke-dasharray="0"
                                                            class="apexcharts-radialbar-area"
                                                            data:pathOrig="M 107.5 40.72926829268293 A 66.77073170731707 66.77073170731707 0 1 1 107.48834630893657 40.72926930965933">
                                                        </path>
                                                    </g>
                                                    <g id="SvgjsG1349"
                                                        class="apexcharts-radialbar-track apexcharts-track" rel="4">
                                                        <path id="apexcharts-radialbarTrack-3"
                                                            d="M 107.5 50.617073170731715 A 56.882926829268285 56.882926829268285 0 1 1 107.49007205643697 50.61707403710823"
                                                            fill="none" fill-opacity="1" stroke="rgba(242,242,242,0.85)"
                                                            stroke-opacity="1" stroke-linecap="butt"
                                                            stroke-width="4.741170731707319" stroke-dasharray="0"
                                                            class="apexcharts-radialbar-area"
                                                            data:pathOrig="M 107.5 50.617073170731715 A 56.882926829268285 56.882926829268285 0 1 1 107.49007205643697 50.61707403710823">
                                                        </path>
                                                    </g>
                                                </g>
                                                <g id="SvgjsG1351">
                                                    <g id="SvgjsG1356"
                                                        class="apexcharts-series apexcharts-radial-series"
                                                        seriesName="Bronze" rel="1" data:realIndex="0">
                                                        <path id="SvgjsPath1357"
                                                            d="M 107.5 20.953658536585365 A 86.54634146341463 86.54634146341463 0 0 1 139.92083014822003 187.74437047670216"
                                                            fill="none" fill-opacity="0.85"
                                                            stroke="rgba(255,108,76,0.85)" stroke-opacity="1"
                                                            stroke-linecap="butt" stroke-width="4.887804878048783"
                                                            stroke-dasharray="0"
                                                            class="apexcharts-radialbar-area apexcharts-radialbar-slice-0"
                                                            data:angle="158" data:value="44" index="0" j="0"
                                                            data:pathOrig="M 107.5 20.953658536585365 A 86.54634146341463 86.54634146341463 0 0 1 139.92083014822003 187.74437047670216">
                                                        </path>
                                                    </g>
                                                    <g id="SvgjsG1358"
                                                        class="apexcharts-series apexcharts-radial-series"
                                                        seriesName="Silver" rel="2" data:realIndex="1">
                                                        <path id="SvgjsPath1359"
                                                            d="M 107.5 30.84146341463415 A 76.65853658536585 76.65853658536585 0 1 1 83.81120943120831 180.40660074916264"
                                                            fill="none" fill-opacity="0.85"
                                                            stroke="rgba(108,117,125,0.85)" stroke-opacity="1"
                                                            stroke-linecap="butt" stroke-width="4.887804878048783"
                                                            stroke-dasharray="0"
                                                            class="apexcharts-radialbar-area apexcharts-radialbar-slice-1"
                                                            data:angle="198" data:value="55" index="0" j="1"
                                                            data:pathOrig="M 107.5 30.84146341463415 A 76.65853658536585 76.65853658536585 0 1 1 83.81120943120831 180.40660074916264">
                                                        </path>
                                                    </g>
                                                    <g id="SvgjsG1360"
                                                        class="apexcharts-series apexcharts-radial-series"
                                                        seriesName="Gold" rel="3" data:realIndex="2">
                                                        <path id="SvgjsPath1361"
                                                            d="M 107.5 40.72926829268293 A 66.77073170731707 66.77073170731707 0 1 1 49.10100218866318 139.87109308259446"
                                                            fill="none" fill-opacity="0.85"
                                                            stroke="rgba(255,193,7,0.85)" stroke-opacity="1"
                                                            stroke-linecap="butt" stroke-width="4.887804878048783"
                                                            stroke-dasharray="0"
                                                            class="apexcharts-radialbar-area apexcharts-radialbar-slice-2"
                                                            data:angle="241" data:value="67" index="0" j="2"
                                                            data:pathOrig="M 107.5 40.72926829268293 A 66.77073170731707 66.77073170731707 0 1 1 49.10100218866318 139.87109308259446">
                                                        </path>
                                                    </g>
                                                    <g id="SvgjsG1362"
                                                        class="apexcharts-series apexcharts-radial-series"
                                                        seriesName="Platinum" rel="4" data:realIndex="3">
                                                        <path id="SvgjsPath1363"
                                                            d="M 107.5 50.617073170731715 A 56.882926829268285 56.882926829268285 0 1 1 57.749071195353686 79.92260984540226"
                                                            fill="none" fill-opacity="0.85"
                                                            stroke="rgba(69,103,226,0.85)" stroke-opacity="1"
                                                            stroke-linecap="butt" stroke-width="4.887804878048783"
                                                            stroke-dasharray="0"
                                                            class="apexcharts-radialbar-area apexcharts-radialbar-slice-3"
                                                            data:angle="299" data:value="83" index="0" j="3"
                                                            data:pathOrig="M 107.5 50.617073170731715 A 56.882926829268285 56.882926829268285 0 1 1 57.749071195353686 79.92260984540226">
                                                        </path>
                                                    </g>
                                                    <circle id="SvgjsCircle1352" r="49.51234146341463" cx="107.5"
                                                        cy="107.5" class="apexcharts-radialbar-hollow"
                                                        fill="transparent"></circle>
                                                    <g id="SvgjsG1353" class="apexcharts-datalabels-group"
                                                        transform="translate(0, 0) scale(1)" style="opacity: 1;">
                                                        <text id="SvgjsText1354"
                                                            font-family="Helvetica, Arial, sans-serif" x="107.5"
                                                            y="107.5" text-anchor="middle" dominant-baseline="auto"
                                                            font-size="16px" font-weight="600" fill="#373d3f"
                                                            class="apexcharts-text apexcharts-datalabel-label"
                                                            style="font-family: Helvetica, Arial, sans-serif;">Total</text><text
                                                            id="SvgjsText1355"
                                                            font-family="Helvetica, Arial, sans-serif" x="107.5"
                                                            y="139.5" text-anchor="middle" dominant-baseline="auto"
                                                            font-size="16px" font-weight="400" fill="#373d3f"
                                                            class="apexcharts-text apexcharts-datalabel-value"
                                                            style="font-family: Helvetica, Arial, sans-serif;">62.25%</text>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                        <line id="SvgjsLine1364" x1="0" y1="0" x2="215" y2="0" stroke="#b6b6b6"
                                            stroke-dasharray="0" stroke-width="1" class="apexcharts-ycrosshairs">
                                        </line>
                                        <line id="SvgjsLine1365" x1="0" y1="0" x2="215" y2="0" stroke-dasharray="0"
                                            stroke-width="0" class="apexcharts-ycrosshairs-hidden"></line>
                                    </g>
                                    <g id="SvgjsG1336" class="apexcharts-annotations"></g>
                                </svg></div>
                        </div>
                        <div class="resize-triggers">
                            <div class="expand-trigger">
                                <div style="width: 252px; height: 349px;"></div>
                            </div>
                            <div class="contract-trigger"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title mb-4">Visitor by Country</h6>

                        <div class="mt-3">
                            <table class="table table-sm align-middle mb-1">
                                <thead class="sm-head">
                                    <tr>
                                        <th>Flag</th>
                                        <th>Name</th>
                                        <th>Visitor</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><img src="assets/img/usa_icon.png" class="rounded-circle" width="25">
                                        </td>
                                        <td>United States</td>
                                        <td width="20"><strong>129</strong></td>
                                    </tr>
                                    <tr>
                                        <td><img src="assets/img/japan_icon.png" class="rounded-circle" width="25">
                                        </td>
                                        <td>Japan</td>
                                        <td><strong>100</strong></td>
                                    </tr>
                                    <tr>
                                        <td><img src="assets/img/korea_icon.png" class="rounded-circle" width="25">
                                        </td>
                                        <td>Korea</td>
                                        <td><strong>80</strong></td>
                                    </tr>
                                    <tr>
                                        <td><img src="assets/img/spain_icon.png" class="rounded-circle" width="25">
                                        </td>
                                        <td>Spain</td>
                                        <td><strong>70</strong></td>
                                    </tr>
                                    <tr>
                                        <td><img src="assets/img/brazil_icon.png" class="rounded-circle" width="25">
                                        </td>
                                        <td>Brazil</td>
                                        <td><strong>60</strong></td>
                                    </tr>
                                    <tr>
                                        <td><img src="assets/img/uk_icon.png" class="rounded-circle" width="25">
                                        </td>
                                        <td>United Kingdom</td>
                                        <td><strong>50</strong></td>
                                    </tr>
                                    <tr>
                                        <td><img src="assets/img/indonesia_icon.png" class="rounded-circle" width="25">
                                        </td>
                                        <td>Indonesia</td>
                                        <td><strong>40</strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <h6 class="card-title mb-4">Upcoming Event</h6>

                        <div class="row event-list-item mt-4">
                            <div class="col-4">
                                <div class="rounded bg-main text-center pt-1 pb-1">
                                    <div class="fs-sm">MAY</div>
                                    <div class="fs-2hr font-weight-light">14</div>
                                    <div class="fs-sm">SUNDAY</div>
                                </div>
                            </div>
                            <div class="col-8 pl-0">
                                <h6><a href="#">Elephant kind live in California</a></h6>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                        </div>
                        <div class="row event-list-item mt-3">
                            <div class="col-4">
                                <div class="rounded bg-info text-center pt-1 pb-1">
                                    <div class="fs-sm">MAY</div>
                                    <div class="fs-2hr font-weight-light">24</div>
                                    <div class="fs-sm">MONDAY</div>
                                </div>
                            </div>
                            <div class="col-8 pl-0">
                                <h6><a href="#">Gallery of the World</a></h6>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                        </div>
                        <div class="row event-list-item mt-3">
                            <div class="col-4">
                                <div class="rounded bg-warning text-center pt-1 pb-1">
                                    <div class="fs-sm">JUN</div>
                                    <div class="fs-2hr font-weight-light">28</div>
                                    <div class="fs-sm">THURSDAY</div>
                                </div>
                            </div>
                            <div class="col-8 pl-0">
                                <h6><a href="#">We The Fest 2020</a></h6>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="sales-order-wrapper mt-4">
            <div class="card">
                <div class="card-body">
                    <h6 class="card-title">Sales Order</h6>

                    <div class="mt-4 table-responsive">
                        <table class="table table-md align-middle table-zoom-hover no-border">
                            <thead class="md-head">
                                <tr>
                                    <th>No Order</th>
                                    <th>Status</th>
                                    <th style="width:180px;">Sales</th>
                                    <th>Customer</th>
                                    <th>Total</th>
                                    <th>Order Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>#66453</td>
                                    <td><span
                                            class="badge badge-pill badge-md font-weight-light badge-primary">Open</span>
                                    </td>
                                    <td>
                                        <div class="multiple-avatar-icon">
                                            <img src="assets/img/avatar_2.jpeg" class="rounded-circle" width="30"
                                                data-toggle="tooltip" title="" data-original-title="Alexander Thomson">
                                            <img src="assets/img/avatar_3.jpeg" class="rounded-circle" width="30"
                                                data-toggle="tooltip" title="" data-original-title="Michael Bara">
                                            <img src="assets/img/avatar_4.jpeg" class="rounded-circle" width="30"
                                                data-toggle="tooltip" title="" data-original-title="Gerald Olando">
                                            <small>2+</small>
                                        </div>
                                    </td>
                                    <td>John Doe</td>
                                    <td>$24.00</td>
                                    <td>2020/05/23</td>
                                    <td>
                                        <a href="#" class="btn btn-xs btn-light mr-2" data-toggle="tooltip" title=""
                                            data-original-title="View Detail"><i class="icon-eye"></i></a>
                                        <a href="#" class="btn btn-xs btn-danger" data-toggle="tooltip" title=""
                                            data-original-title="Cancel Order"><i class="bx bxs-trash"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>#66453</td>
                                    <td><span
                                            class="badge badge-pill badge-md font-weight-light badge-success">Closed</span>
                                    </td>
                                    <td>
                                        <div class="multiple-avatar-icon">
                                            <img src="assets/img/avatar_2.jpeg" class="rounded-circle" width="30"
                                                data-toggle="tooltip" title="" data-original-title="Alexander Thomson">
                                            <img src="assets/img/avatar_3.jpeg" class="rounded-circle" width="30"
                                                data-toggle="tooltip" title="" data-original-title="Michael Bara">
                                            <img src="assets/img/avatar_4.jpeg" class="rounded-circle" width="30"
                                                data-toggle="tooltip" title="" data-original-title="Alexander Thomson">
                                            <img src="assets/img/avatar_5.jpeg" class="rounded-circle" width="30"
                                                data-toggle="tooltip" title="" data-original-title="Gerald Olando">
                                            <small>2+</small>
                                        </div>
                                    </td>
                                    <td>Alexander Luxor</td>
                                    <td>$19.80</td>
                                    <td>2020/05/23</td>
                                    <td>
                                        <a href="#" class="btn btn-xs btn-light mr-2" data-toggle="tooltip" title=""
                                            data-original-title="View Detail"><i class="icon-eye"></i></a>
                                        <a href="#" class="btn btn-xs btn-danger" data-toggle="tooltip" title=""
                                            data-original-title="Cancel Order"><i class="bx bxs-trash"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>#66453</td>
                                    <td><span class="badge badge-pill badge-md font-weight-light badge-warning">On
                                            Progress</span></td>
                                    <td>
                                        <div class="multiple-avatar-icon">
                                            <img src="assets/img/avatar_2.jpeg" class="rounded-circle" width="30"
                                                data-toggle="tooltip" title="" data-original-title="Alexander Thomson">
                                            <img src="assets/img/avatar_3.jpeg" class="rounded-circle" width="30"
                                                data-toggle="tooltip" title="" data-original-title="Micahel Bara">
                                            <img src="assets/img/avatar_4.jpeg" class="rounded-circle" width="30"
                                                data-toggle="tooltip" title="" data-original-title="Alexander Thomson">
                                            <img src="assets/img/avatar_5.jpeg" class="rounded-circle" width="30"
                                                data-toggle="tooltip" title="" data-original-title="Micahel Bara">
                                            <img src="assets/img/avatar.jpeg" class="rounded-circle" width="30"
                                                data-toggle="tooltip" title="" data-original-title="Alexander Thomson">
                                        </div>
                                    </td>
                                    <td>Boeniva Duanoe</td>
                                    <td>$32.00</td>
                                    <td>2020/05/23</td>
                                    <td>
                                        <a href="#" class="btn btn-xs btn-light mr-2" data-toggle="tooltip" title=""
                                            data-original-title="View Detail"><i class="icon-eye"></i></a>
                                        <a href="#" class="btn btn-xs btn-danger" data-toggle="tooltip" title=""
                                            data-original-title="Cancel Order"><i class="bx bxs-trash"></i></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>#66453</td>
                                    <td><span
                                            class="badge badge-pill badge-md font-weight-light badge-success">Closed</span>
                                    </td>
                                    <td>
                                        <div class="multiple-avatar-icon">
                                            <img src="assets/img/avatar_2.jpeg" class="rounded-circle" width="30"
                                                data-toggle="tooltip" title="" data-original-title="Alexander Thomson">
                                            <img src="assets/img/avatar_3.jpeg" class="rounded-circle" width="30"
                                                data-toggle="tooltip" title="" data-original-title="Michael Bara">
                                            <img src="assets/img/avatar_4.jpeg" class="rounded-circle" width="30"
                                                data-toggle="tooltip" title="" data-original-title="Alexander Thomson">
                                            <img src="assets/img/avatar_5.jpeg" class="rounded-circle" width="30"
                                                data-toggle="tooltip" title="" data-original-title="Michael Bara">
                                            <small>2+</small>
                                        </div>
                                    </td>
                                    <td>Alexander Luxor</td>
                                    <td>$14.00</td>
                                    <td>2020/05/23</td>
                                    <td>
                                        <a href="#" class="btn btn-xs btn-light mr-2" data-toggle="tooltip" title=""
                                            data-original-title="View Detail"><i class="icon-eye"></i></a>
                                        <a href="#" class="btn btn-xs btn-danger" data-toggle="tooltip" title=""
                                            data-original-title="Cancel Order"><i class="bx bxs-trash"></i></a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <nav class="mt-4" aria-label="Table navigation">
                            <ul class="pagination">
                                <li class="page-item"><a class="page-link" href="#"><i
                                            class="icon-chevron-left"></i></a></li>
                                <li class="page-item"><a class="page-link" href="#">1</a></li>
                                <li class="page-item active"><a class="page-link" href="#">2</a></li>
                                <li class="page-item"><a class="page-link" href="#">3</a></li>
                                <li class="page-item"><a class="page-link" href="#">4</a></li>
                                <li class="page-item"><a class="page-link" href="#">...</a></li>
                                <li class="page-item"><a class="page-link" href="#">12</a></li>
                                <li class="page-item"><a class="page-link" href="#"><i
                                            class="icon-chevron-right"></i></a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<!-- end of container -->
@endsection

@push('apexchartcss')
<link rel="stylesheet" href="{{ asset('libs/apexcharts/apexcharts.css') }}">
@endpush
@push('apexchartjs')
<script src="{{ asset('libs/apexcharts/apexcharts.min.js') }}"></script>
@endpush