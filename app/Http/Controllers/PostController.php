<?php

namespace App\Http\Controllers;

use App\Post;
use App\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use File;
use Alert;

class PostController extends Controller
{

    /**
     * 
     * check middleware
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * 
     * Menampilkan Halaman utama dari posts
     */
    public function index()
    {
        // menampilkan halaman utama posts dan kirim data 
        $post = Post::paginate(8);
        return view('dashboard.post.index', compact('post'));
    }

    /**
     * 
     * menampilkan halaman input post
     */
    public function create()
    {
        // menampilkan halaman input post dan kirim data 
        $category = Category::all();
        $post = Post::paginate(5);
        return view('dashboard.post.create', compact('category', 'post'));
    }

    /**
     * 
     * untuk memasukan data post
     */
    public function store(Request $request)
    {
        // cek validasi dari request form
        $request->validate([
            'title' => 'required',
            'content' => 'required',
            'posts_image' => 'image|dimensions:min_width=500, min_height=500',
            'category_id' => 'required',
        ]);

        // cek bila ada gambar yang di masukan
        if ($file = $request->file('posts_image')) {
            $filename = now() . $file->getClientOriginalName();
            $target_path = public_path('/img');
            $file->move($target_path, $filename);
        } else {
            $filename = asset('img/default.png');
        }

        // insert data post
        Post::create([
            'title' => $request->title,
            'content' => $request->content,
            'posts_slug' => Str::slug($request->title, '-'),
            'posts_image' => asset('img/' . $filename),
            'category_id' => $request->category_id
        ]);

        // kembali ke halaman sebelumnya dan tampilkan alert
        Alert::success('berhasil', 'Data Berhasil Di Tambahkan!');
        return redirect()->route('post.create');
    }

    /**
     *
     * menampilkan halaman edit post
     */
    public function edit(Post $post)
    {
        // menampilkan halaman edit post dan kirim data 
        $category = Category::all();
        return view('dashboard.post.edit', compact('post', 'category'));
    }

    /**
     * 
     * untuk update data post
     */
    public function update(Request $request, Post $post)
    {
        // cek validasi dari request form
        $request->validate([
            'title' => 'required',
            'content' => 'required',
            'posts_image' => 'image|dimensions:min_width=500, min_height=500',
            'category_id' => 'required',
        ]);

        // cek jika ada gambar yang di update
        if ($file = $request->file('posts_image')) {
            if ($post->posts_image !== 'default.png') {
                File::delete('img/' . $post->posts_image);
            }
            $filename = asset('img/' . now() . $file->getClientOriginalName());
            $target_path = public_path('/img');
            $file->move($target_path, $filename);
        } else {
            $filename =  $post->posts_image;
        }

        // update data post
        Post::where('id', $post->id)
            ->update([
                'title' => $request->title,
                'content' => $request->content,
                'posts_slug' => Str::slug($request->title, '-'),
                'posts_image' => $filename,
                'category_id' => $request->category_id
            ]);

        // kembali ke halaman sebelumnya dan tampilkan alert
        Alert::success('berhasil', 'Data Berhasil Di Ubah!');
        return redirect()->route('post.index');
    }

    /**
     * 
     * untuk delete data post
     */
    public function destroy(Post $post)
    {
        // hapus data kategori berdasarkan data yang di kirim di form 
        Post::destroy($post->id);
        // hapus gambar jika bukan default.png 
        if ($post->posts_image !== 'default.png') {
            File::delete('img/' . $post->posts_image);
        }
        // kembali ke halaman sebelumnya dan tampilkan alert
        Alert::success('berhasil', 'Data Berhasil Di Hapus!');
        return redirect()->route('post.index');
    }
}
