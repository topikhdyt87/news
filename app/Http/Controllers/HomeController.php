<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\User;
use Illuminate\Http\Request;


class HomeController extends Controller
{
    /**
     * check middleware
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * 
     * menampilkan halaman utama / Dashboard
     */
    public function index()
    {
        //  menampilkan halaman dashboard dan kirim semua data 
        $category = Category::all()->count();
        $post = Post::all()->count();
        return view('dashboard.index', compact('category', 'post', 'user'));
    }
}
