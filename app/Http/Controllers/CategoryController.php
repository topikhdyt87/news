<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Alert;


class CategoryController extends Controller
{
    /**
     * check middleware
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * 
     * Menampilkan Halaman Utama Category
     */
    public function index()
    {
        // menampilkan halaman kategori dan kirim semua data kategori
        $category = Category::paginate(5);
        return view('dashboard.category.index', compact('category'));
    }

    /**
     * 
     * Untuk Memasukan data Categori
     */
    public function store(Request $request)
    {
        // cek validasi dari request form
        $request->validate([
            'title' => 'required'
        ]);

        // insert data kategori
        Category::create([
            'title' => $request->title,
            'cat_slug' => Str::slug($request->title, '-')
        ]);

        // kembali ke halaman sebelumnya dan tampilkan alert
        Alert::success('Berhasil!', 'Data Berhasil Di Tambahkan!');
        return back();
    }

    /**
     * 
     * Untuk Update Data Category
     */
    public function update(Request $request, Category $category)
    {
        // cek validasi dari request form
        $request->validate([
            'title' => 'required'
        ]);

        // update data kategori
        Category::where('id', $category->id)
            ->update([
                'title' => $request->title,
                'cat_slug' => Str::slug($request->title, '-')
            ]);

        // kembali ke halaman sebelumnya dan tampilkan alert
        Alert::success('Berhasil!', 'Data Berhasil Di Update!');
        return back();
    }

    /**
     * 
     *  untuk delete data category
     * ! Check bila data tidak bisa di hapus 
     */
    public function destroy(Category $category)
    {
        // hapus data kategori berdasarkan data yang di kirim di form 
        Category::destroy($category->id);

        // kembali ke halaman sebelumnya dan tampilkan alert
        Alert::success('Berhasil!', 'Data Berhasil Di Delete!');
        return back();
    }
}
