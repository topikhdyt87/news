<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Post;

class PostController extends Controller
{

    /**
     * for request all data post and headline (with category)
     */
    public function index()
    {
        // Prepare data for Request Client 
        $headline = Post::orderBy('created_at', 'DESC')->first();
        $post = Post::whereNotIn('id', [$headline->id])->paginate(3);
        
        // make array all data 
        $headline = $headline->toArray();
        $posts = $post->toArray();

        // prepare response
        $res = [
            'success' => true,
            'headline' => $headline,
            'data' => $posts,
            'message' => 'Post Retrivied Successfully'
        ];

        // return response data
        return response()->json($res, 200);
    }

    /**
     *  for getting data specified slug
     */
    public function show($slug)
    {
        //  prepare data for Request Client
        $detail = Post::where('posts_slug', $slug)->with('category')->first();
        $post =  Post::whereNotIn('id', [$detail->id])->paginate(3);

        //  check if data not available 
        if (is_null($post)) {

            // prepare response if data not available
            $res = [
                'success' => false,
                'data' => 'Empty',
                'message' => 'Posts Not Found',
            ];

            // = return if not available 
            return response()->json($res, 200);
        }

        // make array if data available
        $posts = $post->toArray();
        $details = $detail->toArray();

        // prepare response
        $res = [
            'success' => true,
            'detail' => $details,
            'data' => $posts,
            'Message' => 'Post Retrievied Successfully',
        ];

        // return response data
        return response()->json($res, 200);
    }
}
