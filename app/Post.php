<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = ['title' ,'content', 'posts_slug','posts_image', 'category_id'];

    public function category() 
    {
        return $this->belongsTo(Category::class, 'category_id', 'id');
    }
}
